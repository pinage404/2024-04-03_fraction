import { expect, it } from "./dev_deps.ts";

it("multiply", () => {
  expect(multiply(fraction(1, 1), fraction(1, 1))).toEqual(fraction(1, 1));
});

it("multiply 2", () => {
  expect(multiply(fraction(2, 1), fraction(1, 1))).toEqual(fraction(2, 1));
});

it("multiply 3", () => {
  expect(multiply(fraction(2, 1), fraction(2, 1))).toEqual(fraction(4, 1));
});

it("multiply 4", () => {
  expect(multiply(fraction(1, 2), fraction(1, 1))).toEqual(fraction(1, 2));
});

it("divide", () => {
  expect(divide(fraction(1, 1), fraction(1, 1))).toEqual(fraction(1, 1));
});

it("divide 2", () => {
  expect(divide(fraction(1, 1), fraction(1, 2))).toEqual(fraction(2, 1));
});

class Fraction {
  constructor(public numerator: number, public denominator: number) {}
}

function fraction(numerator: number, denominator: number): Fraction {
  return new Fraction(numerator, denominator);
}
function multiply(a: Fraction, b: Fraction): Fraction {
  return fraction(a.numerator * b.numerator, a.denominator * b.denominator);
}
function divide(a: Fraction, b: Fraction): Fraction {
  return multiply(a, inverse(b));
}
function inverse(input: Fraction): Fraction {
  return fraction(input.denominator, input.numerator);
}
